# Resources #

This repo contains links to resources used by the Olympic Peninsula Web, UI & UX Design group.

Resources which will be posted here include:

* **Learning resources**  
* **3rd-party libraries**  
* **Group agenda/goals**  