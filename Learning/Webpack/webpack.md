## Webpack Resources ##

### Some useful links to learning webpack: ###

**[Mindspace (Max Schwarzmueller) Webpack 2 Basics Tutorial playlist](https://www.youtube.com/playlist?list=PL55RiY5tL51rcCnrOrZixuOsZhAHHy6os)**  

* [WHAT IS WEBPACK, HOW DOES IT WORK?](https://www.youtube.com/watch?v=GU-2T7k9NfI)  
* [USING THE WEBPACK DEV SERVER](https://www.youtube.com/watch?v=HNRt0lODCQM)  
* [THE WEBPACK CORE CONCEPTS](https://www.youtube.com/watch?v=8DDVr6wjJzQ)  
* [BASIC BABEL + SCSS WORKFLOW](https://www.youtube.com/watch?v=8vnkM8JgjpU)  
* [HTML + IMAGE LOADERS](https://www.youtube.com/watch?v=CzLiXgRUt4g)  
* [WEBPACK + MULTIPLE HTML FILES](https://www.youtube.com/watch?v=y_RFOaSDL8I)  
* [USING 3RD PARTY PACKAGES LIKE JQUERY](https://www.youtube.com/watch?v=IYuh8hIyvfE)
  
  
**[Ihatetomatoes (Petr Tichy) Webpack 2 playlist](https://www.youtube.com/playlist?list=PLkEZWD8wbltnRp6nRR8kv97RbpcUdNawY)**  

* [Webpack 2 - Installation and Config](https://www.youtube.com/watch?v=JdGnYNtuEtE)  
* [Webpack 2 - HTML Webpack Plugin](https://www.youtube.com/watch?v=cKTDYSK0ArI)  
* [Webpack 2 - Style, CSS and Sass loaders](https://www.youtube.com/watch?v=m7V0OackwxY) 
* [Webpack 2 - Webpack Dev Server](https://www.youtube.com/watch?v=gH4LxB6NkNc) 
* [Webpack 2 - Webpack Dev Server Configuration](https://www.youtube.com/watch?v=soI7X-7OSb4) 
* [Webpack 2 - How to install React and Babel](https://www.youtube.com/watch?v=zhA5LNA3MxE) 
* [Webpack 2 - Multiple templates options and RimRaf](https://www.youtube.com/watch?v=OvjB2Sfq9ZU)    
* [Webpack 2 - How to use pug (jade) templates with Webpack](https://www.youtube.com/watch?v=nLR5yUCjTis) 
* [Webpack 2 - Hot Module Replacement - CSS](https://www.youtube.com/watch?v=faFJw1wjQLE) 
* [Webpack 2 - Production vs Development Environment](https://www.youtube.com/watch?v=NtyzJMi-a-M) 
* [Webpack 2 - How to load images with Webpack 2](https://www.youtube.com/watch?v=cDLfpth5a3s) 
* [Webpack 2 - How to load Twitter Bootstrap with Webpack 2](https://www.youtube.com/watch?v=cN0eUhoV_Gc) 
* [Webpack 2 - How to optimize your css stylesheet with Webpack 2](https://www.youtube.com/watch?v=-WRjUQG4huA) 